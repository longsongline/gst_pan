本项目主要用于识别
此类型的验证码：鱼眼+红线

![img.png](img.png)

0.99 model的模型已经训练好了

修改验证码路径： 
process.py 里面的 t = file_name("./yzm4")
getimg.py 里面的两处

修改模型路径：
checkpoint 第一行

修改完毕，直接运行interfaces.py 即可
查询接口：
/pan      {“pan”：xxxx}
/gst      {“gst”：xxxx}