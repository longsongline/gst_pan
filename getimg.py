import os
from PIL import Image
import numpy as np

CAPTCHA_LEN = 6

CAPTCHA_HEIGHT = 50

CAPTCHA_WIDTH = 182

NUMBER = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

CAPTCHA_LIST = NUMBER


def file_name(file_dir):
    for files in os.walk(file_dir):
        return files[2]  # 当前路径下所有非目录子文件


def load_image(filename, isFlatten=False):
    isExit = os.path.isfile(filename)
    if isExit == False:
        print("打开失败 ")
    img = Image.open(filename)

    if isFlatten:
        img_flatten = np.array(np.array(img).flatten())
        # print(img_flatten)
        return img_flatten
    else:
        img_arr = np.array(img)
        # print(img_arr)
        return img_arr


def load_allimg():
    flies = file_name(r".\yzm4")  # 这里填写验证码数据集的路径
    list1 = []

    list1.append(load_image(r".\yzm4\10001.png"))
    list1.append(load_image(r".\yzm4\10002.png"))# 这里填写验证码数据集的路径
    return list1
