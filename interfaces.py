# -*- coding: UTF-8 -*-
import json
from time import sleep
import json
import os
from time import sleep
import tensorflow.compat.v1 as tf
from dotenv import load_dotenv, find_dotenv
from pathlib import Path
import os

import socket
tf.disable_v2_behavior()
from train import cnn_graph
from process import vec2text, convert2gray, wrap_gen_captcha_text_and_image
from getimg import CAPTCHA_HEIGHT, CAPTCHA_WIDTH, CAPTCHA_LEN, CAPTCHA_LIST
import requests
import numpy as np
import requests

import time
import os
import json

os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'
from flask import Flask, request

app = Flask(__name__)

load_dotenv(verbose=True)

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
    'Cookie': 'CaptchaCookie=1c1e71cc51a94638a1f8299f6aee797c; TS01b8883c=019116acc021d5352fc1a5b1d101e96bfe471e2f9da05c960926f35dd74b0ea604c68e5c38623344ded1032f0ca82894657d7a8938',
    'Connection': 'close'
}

proxies = {

    'http': 'http://127.0.0.1:7890',

    'https': 'https://127.0.0.1:7890'

}

def get_url_comment(url):
    headers = {
        "User-Agent":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400"
    }
    try:
        r = requests.get(url ,headers = headers, timeout = 30)#获得url的相关参数
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        print ("网页获取成功",r.status_code )
        return r.text
    except Exception as e :
        return "网页爬取异常" 

def get_host_ip():
    """
    查询本机ip地址
    :return:
    """
    try:
        s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        s.connect(('8.8.8.8',80))
        ip=s.getsockname()[0]
    finally:
        s.close()
 
    return ip

# 验证码图片转化为文本
def captcha2text(image_list, height=CAPTCHA_HEIGHT, width=CAPTCHA_WIDTH):
    x = tf.placeholder(tf.float32, [None, height * width])
    keep_prob = tf.placeholder(tf.float32)
    y_conv = cnn_graph(x, keep_prob, (height, width))
    saver = tf.train.Saver()
    text_lists = []
    with tf.Session() as sess:
        saver.restore(sess, tf.train.latest_checkpoint('.'))
        predict = tf.argmax(tf.reshape(y_conv, [-1, CAPTCHA_LEN, len(CAPTCHA_LIST)]), 2)
        for i in range(len(image_list[0])):
            vector_list = sess.run(predict, feed_dict={x: [image_list[0][i]], keep_prob: 1})
            vector_list = vector_list.tolist()
            text_list = [vec2text(vector) for vector in vector_list]
            text_lists.append(text_list)
        return text_lists


@app.route('/gst', methods=['POST'])
def indextest():
    url = "https://whois.pconline.com.cn/ip.jsp?ip=" + "".join(str(get_host_ip()))
    ha = get_url_comment(url)
    print(os.getenv('API_PROXY_ENABLED'))
    inputData = request.json.get('gst')
    data1 = getcontent(inputData)
    return data1


@app.route('/pan', methods=['POST'])
def indextest1():
    url = "https://whois.pconline.com.cn/ip.jsp?ip=" + "".join(str(get_host_ip()))
    ha = get_url_comment(url)
    print(os.getenv('API_PROXY_ENABLED'))
    inputData = request.json.get('pan')
    data1 = getcontent1(inputData)
    return data1


def getcontent(inputData):
    ha = [r".\yzm4\10001.png", r".\yzm4\10002.png"]
    coo = []
    for i in range(2):
        if (os.getenv('API_PROXY_ENABLED')!='true'):
            myfile = requests.get(url="https://services.gst.gov.in/services/captcha", headers=headers,
                              verify=False)
        else:
            myfile = requests.get(url="https://services.gst.gov.in/services/captcha", headers=headers,proxies=proxies,
                              verify=False)
        print("cookie:", myfile.cookies.items()[0][1])
        coo.append(myfile.cookies.items()[0][1])
        with open(ha[i], 'wb') as f:
            f.write(myfile.content)

    headers1 = {
        'Content-Type': 'application/json',
        'Cookie': 'CaptchaCookie=' + str(myfile.cookies.items()[0][
                                             1]) + ';TS01b8883c=019116acc091f6d8aa85b58ea4e09e7c378132318186cebe4ba8792436897e6fed4204677845bd10e88af6126b6e444c9ceeff6a5eak_bmsc=24D7275743E9DA089BADC79ABF0291CF~000000000000000000000000000000~YAAQR/ferQzpocqCAQAA2bCn9hAKViEs9JSlPVdZGFNSrbOGKLWur0ymg6Pt3GYLkfOR4APH//i3k9aFuj45B9azte4Dn8o08Ji39a6dlt5QkZvTec7goNK4fkjmLIb0I47HbjhKUPtrSKW8VJzspQRalAgcOG8S1hYR14vpGaTUKVwCUmwRzIekNvmG4u3Bh1RYaik2jztmRaQLrMUCYtI5MYgtzH/M82bb/Ge3J/oPO82CMIFqlcxJLL7E03H5aw5ZPvjHCq7yu1YoGQurtifuw0cGkyxb8gmbFsceg4YAA5BngUqyZoujRZbNHurqHgKC/T2qifxEoRrnzupCd9Z6qD+MRiKTMH/KGB/BZU9KNP8m3u1EFb02QCDNXl2FH3I=',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
    }
    text, image = wrap_gen_captcha_text_and_image()

    acc = 1
    img_array = []
    for i in range(len(image)):
        img_array.append(np.array(image[i]))
    image = convert2gray(img_array)

    for i in range(len(image)):
        image[i] = image[i].flatten() / 255

    pre_text = captcha2text([image])

    for i in range(len(image)):
        if pre_text[i][0] == text[i]:
            print(' 正确验证码:', text[i], "识别出来的：", pre_text[i], "  TURE")
            acc = acc + 1
        else:
            print(' 正确验证码:', text[i], "识别出来的：", pre_text[i], "FLASE")
    print("正确率：", acc)
    for i in range(2):
        headers1 = {
            'Content-Type': 'application/json',
            'Cookie': 'CaptchaCookie=' + str(coo[i]) + ';TS01b8883c=019116acc091f6d8aa85b58ea4e09e7c378132318186cebe4ba8792436897e6fed4204677845bd10e88af6126b6e444c9ceeff6a5eak_bmsc=24D7275743E9DA089BADC79ABF0291CF~000000000000000000000000000000~YAAQR/ferQzpocqCAQAA2bCn9hAKViEs9JSlPVdZGFNSrbOGKLWur0ymg6Pt3GYLkfOR4APH//i3k9aFuj45B9azte4Dn8o08Ji39a6dlt5QkZvTec7goNK4fkjmLIb0I47HbjhKUPtrSKW8VJzspQRalAgcOG8S1hYR14vpGaTUKVwCUmwRzIekNvmG4u3Bh1RYaik2jztmRaQLrMUCYtI5MYgtzH/M82bb/Ge3J/oPO82CMIFqlcxJLL7E03H5aw5ZPvjHCq7yu1YoGQurtifuw0cGkyxb8gmbFsceg4YAA5BngUqyZoujRZbNHurqHgKC/T2qifxEoRrnzupCd9Z6qD+MRiKTMH/KGB/BZU9KNP8m3u1EFb02QCDNXl2FH3I=',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
        }
        pre = {"gstin": str(inputData), "captcha": pre_text[i][0]}
        if (os.getenv('API_PROXY_ENABLED')!='true'):
            chsihi = requests.post(url="https://services.gst.gov.in/services/api/search/taxpayerDetails", headers=headers1,
                               data=json.dumps(pre), verify=False)
        else:
            chsihi = requests.post(url="https://services.gst.gov.in/services/api/search/taxpayerDetails", headers=headers1,proxies=proxies,data=json.dumps(pre), verify=False)
        if len(chsihi.text) >= 100:
            haha = chsihi.json()
            return haha
    return "错误"


def getcontent1(inputData):
    # conn = cx_Oracle.connect('abc/abd@192.168.123.456/data123')  # 连自己的数据库
    # cur = conn.cursor()
    # sql = "SELECT XXNAME,XXCLASS,XXNUMBER,XXSCORE FROM TEST123 where XXNAME='%s'" % (inputData)
    # cur.execute(sql)
    # data = cur.fetchone()
    # print(data)

    ha = [r".\yzm4\10001.png",r".\yzm4\10002.png"]
    coo = []
    for i in range(2):
        if (os.getenv('API_PROXY_ENABLED')!='true'):
            myfile = requests.get(url="https://services.gst.gov.in/services/captcha", headers=headers,
                              verify=False)
        else:
            myfile = requests.get(url="https://services.gst.gov.in/services/captcha", headers=headers,proxies=proxies,
                              verify=False)
        print("cookie:", myfile.cookies.items()[0][1])
        coo.append(myfile.cookies.items()[0][1])
        with open(ha[i], 'wb') as f:
            f.write(myfile.content)

    headers1 = {
        'Content-Type': 'application/json',
        'Cookie': 'CaptchaCookie=' + str(myfile.cookies.items()[0][1]) + ';TS01b8883c=019116acc091f6d8aa85b58ea4e09e7c378132318186cebe4ba8792436897e6fed4204677845bd10e88af6126b6e444c9ceeff6a5eak_bmsc=24D7275743E9DA089BADC79ABF0291CF~000000000000000000000000000000~YAAQR/ferQzpocqCAQAA2bCn9hAKViEs9JSlPVdZGFNSrbOGKLWur0ymg6Pt3GYLkfOR4APH//i3k9aFuj45B9azte4Dn8o08Ji39a6dlt5QkZvTec7goNK4fkjmLIb0I47HbjhKUPtrSKW8VJzspQRalAgcOG8S1hYR14vpGaTUKVwCUmwRzIekNvmG4u3Bh1RYaik2jztmRaQLrMUCYtI5MYgtzH/M82bb/Ge3J/oPO82CMIFqlcxJLL7E03H5aw5ZPvjHCq7yu1YoGQurtifuw0cGkyxb8gmbFsceg4YAA5BngUqyZoujRZbNHurqHgKC/T2qifxEoRrnzupCd9Z6qD+MRiKTMH/KGB/BZU9KNP8m3u1EFb02QCDNXl2FH3I=',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
    }
    text, image = wrap_gen_captcha_text_and_image()

    acc = 1
    img_array = []
    for i in range(len(image)):
        img_array.append(np.array(image[i]))
    image = convert2gray(img_array)

    for i in range(len(image)):
        image[i] = image[i].flatten() / 255

    pre_text = captcha2text([image])

    for i in range(len(image)):
        if pre_text[i][0] == text[i]:
            print(' 正确验证码:', text[i], "识别出来的：", pre_text[i], "  TURE")
            acc = acc + 1
        else:
            print(' 正确验证码:', text[i], "识别出来的：", pre_text[i], "FLASE")
    print("正确率：", acc)
    for i in range(2):
        headers1 = {
            'Content-Type': 'application/json',
            'Cookie': 'CaptchaCookie=' + str(coo[i]) + ';TS01b8883c=019116acc091f6d8aa85b58ea4e09e7c378132318186cebe4ba8792436897e6fed4204677845bd10e88af6126b6e444c9ceeff6a5eak_bmsc=24D7275743E9DA089BADC79ABF0291CF~000000000000000000000000000000~YAAQR/ferQzpocqCAQAA2bCn9hAKViEs9JSlPVdZGFNSrbOGKLWur0ymg6Pt3GYLkfOR4APH//i3k9aFuj45B9azte4Dn8o08Ji39a6dlt5QkZvTec7goNK4fkjmLIb0I47HbjhKUPtrSKW8VJzspQRalAgcOG8S1hYR14vpGaTUKVwCUmwRzIekNvmG4u3Bh1RYaik2jztmRaQLrMUCYtI5MYgtzH/M82bb/Ge3J/oPO82CMIFqlcxJLL7E03H5aw5ZPvjHCq7yu1YoGQurtifuw0cGkyxb8gmbFsceg4YAA5BngUqyZoujRZbNHurqHgKC/T2qifxEoRrnzupCd9Z6qD+MRiKTMH/KGB/BZU9KNP8m3u1EFb02QCDNXl2FH3I=',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
        }
        pre = {"panNO": str(inputData), "captcha": pre_text[i][0]}
        if (os.getenv('API_PROXY_ENABLED')!='true'):
            chsihi = requests.post(url="https://services.gst.gov.in/services/api/get/gstndtls", headers=headers1,
                               data=json.dumps(pre), verify=False)
        else:
            chsihi = requests.post(url="https://services.gst.gov.in/services/api/get/gstndtls", headers=headers1,proxies=proxies,data=json.dumps(pre), verify=False)
        if len(chsihi.text) >= 100:
            haha = chsihi.json()
            return haha
        
    return "错误"


if __name__ == '__main__':
    try:
        app.run(host='0.0.0.0', port=5590)
    except Exception as e:
        app.run(host='0.0.0.0',port =5591)
